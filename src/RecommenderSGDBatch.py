import logging
import numpy as np
import pandas as pd

from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Recommender import Recommender


class RecommenderSGDBatch(Recommender):
    def __init__(self, data_handler, model):

        super().__init__(data_handler=data_handler,
                         feature_handler=model.feature_handler,
                         model=model)

        self.X = None
        self.y = None

    def fit(self, X, y):
        self.X = X
        self.y = y

        user_features_new = FeatureHandler.build_user_features(X, y, self.feature_handler.movie_features)

        self.model.feature_handler.append_user_features(user_features_new)

    def predict_rating_for_all_movies(self):
        # Build df with all possible combinations
        logging.info('Predicting ratings for all movies')
        userIds = self.X['userId'].unique().tolist()
        movieIds = self.feature_handler.movie_features.index.tolist()

        X_combinations = []
        for userId in userIds:
            for movieId in movieIds:
                X_combinations.append((userId, movieId))
        X_combinations = pd.DataFrame(X_combinations, columns=self.X.columns)

        # Predict rating for all combinations
        #Remove ists just for debug
        #X_combinations = X_combinations.iloc[0:10]

        predictions = X_combinations

        predictions['prediction'] = self.model.predict(X_combinations)

        return predictions

    def _filter_seen_movies(self, df_with_movieId_column):
        seen_movieIds = self.X['movieId']
        return df_with_movieId_column[~df_with_movieId_column['movieId'].isin(seen_movieIds)]

    def recommend_top_n(self, n=8, return_seen_movies=False):
        predictions = self.predict_rating_for_all_movies()

        if not return_seen_movies:
            before = len(predictions)
            predictions = self._filter_seen_movies(predictions)
            logging.info('Filtering out seen movies. Before: {before} After: {after}'.format(before=before,
                                                                                             after=len(predictions)))
        predictions.sort_values(by='prediction', ascending=False, inplace=True)
        return predictions.iloc[0:n]

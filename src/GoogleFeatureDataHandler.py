import os
import io
import logging
import tempfile
import json

import pandas as pd
from joblib import load
from google.cloud import storage, datastore

from src.DataHandler import FeatureDataHandler


class GoogleFeatureDataHandler(FeatureDataHandler):
    def __init__(self):
        super().__init__()
        storage_client = storage.Client(project="app-questionnaire")
        self.bucket = storage_client.get_bucket('app-questionnaire.appspot.com')
        self.path_explanations = 'explanations/'

        self.datastore_client = datastore.Client(project="app-questionnaire")

    def load_model(self, filename="SGDBatchRegressor_subset.joblib"):
        logging.info('Loading model from Cloud Storage')
        blob = self.bucket.get_blob('model/{}'.format(filename))

        try:
            fd, path = tempfile.mkstemp()
            with os.fdopen(fd, 'wb+') as prj_file:
                # do stuff with temp file
                blob.download_to_file(prj_file)
                prj_file.seek(0)
                data = load(prj_file)
        finally:
            os.remove(path)

        return data

    def load_ratings_dev(self):
        logging.info('Loading ratings from Cloud Storage')
        blob = self.bucket.get_blob('data/processed/ml-20m/ratings_dev.csv')

        try:
            fd, path = tempfile.mkstemp()
            with os.fdopen(fd, 'wb+') as prj_file:
                # do stuff with temp file
                blob.download_to_file(prj_file)
                prj_file.seek(0)
                data = pd.read_csv(prj_file)
        finally:
            os.remove(path)

        return data

    def load_explanation(self):
        logging.info('Loading mock_explanation from Cloud Storage')
        blob = self.bucket.get_blob('model/explanations.p')

        try:
            fd, path = tempfile.mkstemp()
            with os.fdopen(fd, 'wb+') as prj_file:
                # do stuff with temp file
                blob.download_to_file(prj_file)
                prj_file.seek(0)
                data = load(prj_file)
        finally:
            os.remove(path)

        return data

    def save_explanation_png_to_datastorage(self, filename, imagedata):
        blob = self.bucket.blob(self.path_explanations + filename)
        blob.upload_from_string(imagedata.getvalue(), content_type='image/png')
        return blob.public_url

    def get_explanation_png(self, filename):
        blob = self.bucket.blob(self.path_explanations + filename)
        buffer = io.BytesIO()
        blob.download_to_file(buffer)
        return buffer

    def save_ratings_to_datastore(self, X, y):
        logging.info('Saving {n} ratings to Datastore'.format(n=len(X)))
        ratings = X.copy()
        ratings['rating'] = y

        for index, rating, in ratings.iterrows():
            key = self.datastore_client.key('Rating', '{userId}_{movieId}'.format(userId=rating['userId'],
                                                                                  movieId=rating['movieId']))
            entity = datastore.Entity(key=key)
            update_dict = {
                'userId': rating['userId'],
                'movieId': rating['movieId'],
                'rating': rating['rating'],
            }
            entity.update(update_dict)
            # Improvement: upload all together

            self.datastore_client.put(entity)

    def save_explanations_to_datastore(self, explanations):
        logging.info('Saving {n} explanations to Datastore'.format(n=len(explanations)))
        for explanation in explanations:
            key = self.datastore_client.key('Explanation', "{userId}_{movieId}".format(userId=explanation['userId'],
                                                                                       movieId=+explanation['movieId']))

            entity = datastore.Entity(key=key)
            update_dict = {
                'userId': explanation['userId'],
                'movieId': explanation['movieId'],
                'timestamp': explanation['timestamp'],
                'predicition': explanation['prediction'],
                'explanation_list': json.dumps(explanation['explanation_list']),
                'description': explanation['description']
            }
            entity.update(update_dict)
            # Improvement: upload all together
            self.datastore_client.put(entity)

    def save_user_features_to_datastore(self, userId, user_features):
        logging.info('Saving user_features to Datastore')
        key = self.datastore_client.key('User_Features', int(userId))
        entity = datastore.Entity(key=key)
        update_dict = {
            'user_features': json.dumps(user_features)
        }
        entity.update(update_dict)
        self.datastore_client.put(entity)

    def load_entity_kind_from_datastore(self,entity_kind):
        logging.info('Loading {entity_kind} from Datastore'.format(entity_kind=entity_kind))
        query = self.datastore_client.query(kind=entity_kind)
        results = list(query.fetch())

        return results

    def load_explanation_from_datastore(self):

        logging.info('Loading explanations from Datastore')
        query = self.datastore_client.query(kind='Explanation')
        results = list(query.fetch())

        return results


import io
import logging
import numpy as np
import pandas as pd
import pickle
import base64

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from src.GoogleFeatureDataHandler import GoogleFeatureDataHandler
from src.FeatureHandler import FeatureHandler
from src.RecommenderSGDBatch import RecommenderSGDBatch
from src.Explainer import UserFeatureRegressionExplainer
from src.TheMovieDBConnector import TheMovieDBConnector

class Controller():
    def __init__(self):
        # setup
        self.data_handler = GoogleFeatureDataHandler()

    def setup(self):
        self.model = self.data_handler.load_model()
        self.feature_handler_fitted = self.model.feature_handler
        self.recommender = RecommenderSGDBatch(self.data_handler, self.model)
        self.the_movie_db_connector = TheMovieDBConnector(self.data_handler)

        self.X = None
        self.y = None
        self.recommendations = None

        self.explainer = UserFeatureRegressionExplainer()

        self.training_data_representative = UserFeatureRegressionExplainer.build_training_data_representative(
            self.feature_handler_fitted,
            n=5000, data_handler=self.data_handler)
        self.feature_names = FeatureHandler.replace_feature_descriptions(
            self.training_data_representative.columns.tolist(),
            self.data_handler)

    def get_intro_movies(self, n=50):
        movieIds = self.data_handler.get_most_popular_movies(n=n)
        movies = self.data_handler.add_movie_information(movieIds)
        movies['movieId'] = movies.index
        # movies = movies.loc[0:n - 1]
        # # movies = movies.loc[0:n-1, ['title', 'genres']]
        return self.parse_pandas_to_flask(movies)

    def recommend_top_n(self, rating_dicts, n=3):
        def parse_ratings_dict_to_pandas(ratings_dict):
            ratings_ = pd.DataFrame(ratings_dict)
            X = ratings_.loc[:, ['userId', 'movieId']].astype(int)
            y = ratings_.loc[:, 'rating'].astype(float)
            timestamp = ratings_.loc[:, 'timestamp'].iloc[0]
            return X, y, timestamp

        self.X, self.y, timestamp = parse_ratings_dict_to_pandas(rating_dicts)
        self.data_handler.save_ratings_to_datastore(self.X, self.y)

        self.recommender.fit(self.X, self.y)  # has side effects on models.feature_handler

        recommendations = self.recommender.recommend_top_n(n, return_seen_movies=False)
        recommendations['timestamp'] = timestamp
        # recommendations['user_features'] = self.model.feature_handler.user_features

        logging.info("Recommendations are {0}".format(recommendations))
        self.recommendations = recommendations
        return self.parse_pandas_to_flask(self.recommendations)  # return notTested

    def get_top_user_features(self, n=10):
        userId = self.X['userId'][0]
        user_features = self.model.feature_handler.user_features.loc[userId]
        user_features.sort_values(ascending=False, inplace=True)

        top_user_features_dict = user_features.iloc[0:n].to_dict()

        self.data_handler.save_user_features_to_datastore(userId, top_user_features_dict)

        return top_user_features_dict

    def draw_top_user_features(self, user_features):
        user_features = pd.Series(user_features).sort_values(ascending=False)
        # data
        feature_names = FeatureHandler.replace_feature_descriptions(
            user_features.index.tolist(),
            self.data_handler)
        y_pos = np.arange(len(feature_names))

        # drawing
        plt.clf()
        plt.rcdefaults()
        fig, ax = plt.subplots()
        ax.barh(y_pos, user_features.values, color='green')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(feature_names)
        ax.invert_yaxis()
        ax.set_xlabel('Präferenz')
        plt.tight_layout()

        # buffer
        figfile = io.BytesIO()
        plt.savefig(figfile, format='png')
        figfile.seek(0)

        return figfile

    def explain_recommendations_with_n_features(self, n=5):
        if self.recommendations is None:
            logging.error('Call Controller.recommend_top_n first')
        self.explainer.fit(self.feature_handler_fitted, self.model.predict, self.training_data_representative,
                           feature_names=self.feature_names,
                           n_explanation_features=n)

        explanations = self.explainer.explain_Xs(self.recommendations[['userId', 'movieId']])

        explanations['prediction'] = self.recommendations['prediction']
        explanations['timestamp'] = self.recommendations['timestamp']
        movie_information = self.data_handler.add_movie_information(explanations['movieId'])
        explanations = explanations.merge(movie_information, on='movieId')

        #add description
        explanations['description'] = self.the_movie_db_connector.get_movie_descriptions(explanations['movieId']).values

        explanations['explanation_list'] = explanations['explanation'].apply(lambda x: x.as_list())
        explanations.reset_index(inplace=True)

        # generate picture and save to db
        self.generate_images(explanations)

        self.explanations = explanations  # no longer used

        explanations_dict = explanations.to_dict(orient='records')
        self.data_handler.save_explanations_to_datastore(explanations_dict)

        return explanations_dict

    def reformat_explanations_for_flask(self, explanations):
        for explanation in explanations:
            del explanation['explanation']
            explanation['prediction'] = 5 if explanation['prediction'] > 5 else round(
                explanation['prediction'] * 2) / 2  # round to nearest 0.5

        return explanations

    def generate_images(self, explanations):

        for index, explanation in explanations.iterrows():
            logging.info('test')
            lime_explanation = explanation['explanation']

            # build fig
            figure = lime_explanation.as_pyplot_figure()
            figure.set
            plt.tight_layout()

            # save fig to buffer
            buffer = io.BytesIO()
            FigureCanvas(figure).print_png(buffer)

            # save buffer to cloud storage
            filename = "{userId}_{movieId}.png".format(userId=explanation['userId'], movieId=explanation['movieId'])
            self.data_handler.save_explanation_png_to_datastorage(filename, buffer)
            buffer.close()

    def get_explanation_image(self, userId, movieId):
        filename = "{userId}_{movieId}.png".format(userId=userId, movieId=movieId)
        buffer = self.data_handler.get_explanation_png(filename)
        return buffer

    def _build_figure(self, lime_explanation):
        fig = lime_explanation.as_pyplot_figure().show()
        plt.plot()
        plt.tight_layout()

        figfile = io.BytesIO()

        plt.savefig(figfile, format='png')
        figfile.seek(0)
        figdata_png = base64.b64encode(figfile.getvalue())

        # canvas = FigureCanvas(fig)
        #
        # canvas.print_png(png_output)
        #
        # output = png_output.getvalue().encode("base64")

        #
        # output = io.BytesIO()
        # figure = lime_explanation.as_pyplot_figure()
        # plt.tight_layout()
        # FigureCanvas(figure).print_png(output)
        #
        # #figure.savefig(output)

        return figdata_png

    def parse_pandas_to_flask(self, pandas_data):
        """

        :param pandas_data: Dataframe or Series
        :return: dict for flask
        """
        return pandas_data.to_dict(orient='records')

    # _________________________ No longer used

    def get_next_explanation(self):
        '''

        :return: array that can be used in flask
        '''

        # To-Do Response when empty
        logging.info(self.recommendations)
        logging.info(self.explanations)
        logging.info(self.explanations_dicts)

        explanation = self.explanations_dicts.pop()
        self.current_explanation = explanation  # workaround

        movie_information = self.data_handler.add_movie_information(explanation['movieId'])
        explanation.update(movie_information)

        explanation['figure'] = self._build_figure(explanation['explanation'])

        return explanation

    def enrich_explanation(self, explanation):
        movie_information = self.data_handler.add_movie_information(explanation['movieId'])
        explanation.update(movie_information)

        explanation['figure'] = self._build_figure(explanation['explanation'])

        return explanation

    def explain_given_recommendation_with_n_features(self, recommendation, n=5):
        """
        The method currently used
        :param recommendation:
        :param n:
        :return:
        """

        self.explainer.fit(self.feature_handler_fitted, self.model.predict, self.training_data_representative,
                           feature_names=self.feature_names,
                           n_explanation_features=n)
        recommendation = pd.Series(recommendation)
        recommendation['explanation'] = self.explainer.explain_X(recommendation[['userId', 'movieId']])

        recommendation['prediction'] = recommendation['prediction']

        movie_information = self.data_handler.add_movie_information(recommendation['movieId'])
        recommendation = recommendation.append(movie_information)

        recommendation['figure'] = self._build_figure(recommendation['explanation'])
        return recommendation.to_dict()

# controller = Controller()
# controller.setup()
#
# controller.recommend_top_n(mock_ratings)
# explanations = controller.explain_recommendations_with_n_features(n=5)
# explanation = explanations.pop()
# exp = explanation['explanation']
# explanation = controller._retrieve_next_explanation()
#
# figure = explanation['figure']


# controller.X = mock_ratings.loc[:, ['userId', 'movieId']].astype(int)
# controller.y = mock_ratings.loc[:, 'rating'].astype(float)


#
# exp = explanation['explanation']

# # intro_movies = controller.get_intro_movies()
#
# # explanations = pickle.load(open('../explanations.p', 'rb'))

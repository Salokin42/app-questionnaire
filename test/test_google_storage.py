from google.cloud import storage
from src.FeatureHandler import FeatureHandler
from src.DataHandler import FeatureDataHandler
from joblib import load
import pandas as pd
import os

#from tempfile import TemporaryFile
import tempfile
fdata_handler = FeatureDataHandler()


storage_client = storage.Client(project="app-questionnaire")
storage_client.list_buckets()
bucket = storage_client.get_bucket('app-questionnaire.appspot.com')

# for bucket in storage_client.list_buckets():
#     print(bucket)


for t in bucket.list_blobs():
    print(t)



blob = bucket.get_blob('model/SGDBatchRegressor_dev.joblib')
#blob.download_to_file(temp_file)
#print(blob.download_as_string())

model = None


#tempfile = TemporaryFile()
try:
    fd, path = tempfile.mkstemp()
    with os.fdopen(fd, 'wb+') as prj_file:
        # do stuff with temp file
        blob.download_to_file(prj_file)
        prj_file.seek(0)
        model = load(prj_file)
finally:
    os.remove(path)


# blob = bucket.get_blob('data/processed/ml-20m/movie_features_raw.csv')
# test = blob.download_as_string()
# #print(blob.download_as_string())
#
# csv = pd.read_csv(test)
#
#
#
# bucket.get
from src.Controller import *
import time
import base64

# input > WebInterface
timestamp = time.time()
mock_ratings = pd.DataFrame(
    {'userId': [999999] * 6, 'movieId': [1, 3, 5, 11, 16, 2391], 'rating': [1, 1, 1, 5, 5, 5],
     'timestamp': [timestamp] * 6})

controller = Controller()
controller.setup()

# For faster debug
controller.feature_handler_fitted.movie_features = controller.feature_handler_fitted.movie_features.iloc[0:10]

controller.recommend_top_n(mock_ratings, n=3)
explanations = controller.explain_recommendations_with_n_features(n=5)
explanation = explanations.pop()
#
# lime_explanation = explanation['explanation']


# image = controller.get_explanation_image(999999, 6)

top_user_features = controller.get_top_user_features(n=10)
controller.draw_top_user_features(top_user_features)
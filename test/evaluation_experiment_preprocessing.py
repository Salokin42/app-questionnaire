##### This file shouldn't be in the app project, but makes it more convenient
import numpy as np
import pandas as pd

from src.GoogleFeatureDataHandler import GoogleFeatureDataHandler

data_handler = GoogleFeatureDataHandler()

explanations = data_handler.load_entity_kind_from_datastore('Explanation')
ratings = data_handler.load_entity_kind_from_datastore('Rating')
user_features = data_handler.load_entity_kind_from_datastore('User_Features')

data = [explanations,ratings,user_features]

def create_df_from_list_of_google_entities(list_of_google_entities):
    list_of_dicts = []

    for entity in list_of_google_entities:
        entity_dict = {key: entity[key] for key in entity.keys()}
        list_of_dicts.append(entity_dict)

    df = pd.DataFrame(list_of_dicts)
    return df

explanations,ratings,user_features = [create_df_from_list_of_google_entities(d) for d in data]




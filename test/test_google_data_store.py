from google.cloud import datastore

from test.test_Controller import *
import pickle

controller = Controller()
controller.setup()
# Create, populate and persist an entity with keyID=1234
client = datastore.Client(project="app-questionnaire")

key = client.key('EntityKind', 1234)
entity = datastore.Entity(key=key)
entity.update({
    'foo': u'bar',
    'baz': 1337,
    'qux': False,
})
client.put(entity)
# Then get by key for this entity
result = client.get(key)
print(result)

explanations = explanations
explanation = explanations.pop()

# explanation_to_store = {key: explanation[key] for key in
#                         ['userId', 'movieId', 'timestamp', 'prediction', 'explanation_list']}

key = client.key('Explanation', explanation['userId']+"_"+explanation['movieId'])
entity = datastore.Entity(key=key)

update_dict = {
    'userId': explanation['userId'],
    'movieId': explanation['movieId'],
    'timestamp': explanation['timestamp'],
    'predicition': explanation['prediction'],
    'explanation_list': str(explanation['explanation_list'])
}

#
# dict = {}
# for i in range(len(explanation['explanation_list'])):
#     dict['explanation_{i}'.format(i=i)] = explanation['explanation_list'][i]
# update_dict['explanation_list'] = dict


entity.update(update_dict)

client.put(entity)
#
# result = client.get(key)
# print(result)

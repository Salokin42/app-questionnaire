# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_python37_render_template]


import random
import string
import time

import logging

from flask import Flask, render_template, request, redirect, url_for, session, Response
from src.Controller import Controller

app = Flask(__name__)
app.secret_key = b'\x88\x82va\xa3W\x04\x9aa\xc6\xaa\xc0\xd7\x03\x145'

controller = Controller()

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)


@app.route('/')  # entry point if used without questback
def root():
    intro_movies = controller.get_intro_movies()
    # TODO: Show newer/ more recent intro movies
    # TODO: Add images to Website
    return render_template('index.html', intro_movies=intro_movies)


@app.route('/questback', methods=['GET'])  # entry point for questback
# http://127.0.0.1:8080/questback?questback_user_counter=0
def questback():
    intro_movies = controller.get_intro_movies()

    # currently there are 3 modes: recommendation_only, with_score, with_explanation, with_explanation_with_userpreference
    mode = request.args['mode']
    session['mode'] = mode

    questback_user_counter = request.args['questback_user_counter']
    userId = int(str(999999) + str(questback_user_counter))
    session['userId'] = userId

    session['request_url'] = request.url

    return render_template('index.html', intro_movies=intro_movies)


@app.route('/recommendation_calc', methods=['GET', 'POST'])
def get_recommendations():
    # HTML Input Form to Dic
    rating_dicts = []

    userId = session['userId'] if ('userId' in session) else ''.join(random.choices(string.digits, k=8))
    timestamp = time.time()

    for movieId in request.form:
        form_rating = request.form[movieId]
        if form_rating is not "":
            rating = {}
            # TODO: Think about userID
            rating['userId'] = userId
            rating['movieId'] = movieId
            rating['rating'] = form_rating
            rating['timestamp'] = timestamp
            rating_dicts.append(rating)

    logging.info("Input rating are: {0}".format(rating_dicts))
    controller.setup()

    # Just for faster debug
    # controller.feature_handler_fitted.movie_features = controller.feature_handler_fitted.movie_features

    controller.recommend_top_n(rating_dicts, n=3)
    explanations = controller.explain_recommendations_with_n_features(n=10)
    explanations = controller.reformat_explanations_for_flask(explanations)

    session['explanations'] = explanations
    session['top_user_features'] = controller.get_top_user_features(n=10)
    # controller.save

    # ____________________
    #     logging.info('Saving recommendations to file')
    #     pickle.dump(session['recommendations']import urllib, open('recommendations_temp.p', 'wb'))
    #
    #     logging.info('Using saved recommendations')
    #     session['recommendations'] = pickle.load(open('recommendations_temp.p', 'rb'))
    # #_____________________________

    if 'mode' not in session:  # if using the app without the questionnaire
        return redirect(url_for('user_features'))
    else:
        mode = session['mode']

    if mode == 'recommendation_only':
        return redirect(url_for('recommendation_only', i=0))
    elif mode == 'with_score':
        return redirect(url_for('with_score', i=0))
    elif mode == 'with_explanation':
        return redirect(url_for('with_explanation', i=0))
    elif mode == 'without_explanation_with_userpreference':
        return redirect(url_for('user_features_link_without_explanation'))
    elif mode == 'with_explanation_with_userpreference':
        return redirect(url_for('user_features'))


def _get_i():
    """
    To handle the number i of the recommendation
    :return:
    """
    i = int(request.args['i'])
    display = {'previous': 'inline',
               'next': 'inline'}
    if i <= 0:
        i = 0
        display['previous'] = 'none'

    elif i >= len(session['explanations']) - 1:
        i = len(session['explanations']) - 1
        display['next'] = 'none'
    return i, display


def _retrieve_explanation_i(i):
    return session['explanations'][i]


def _retrieve_next_explanation():
    explanations = session['explanations']
    explanation = explanations.pop()
    session['explanations'] = explanations
    return explanation


@app.route('/recommendation_only', methods=['GET'])
def recommendation_only():
    i, display = _get_i()
    explanation = _retrieve_explanation_i(i)

    return render_template('recommendation_only.html', explanation=explanation, next=(i + 1), previous=(i - 1),
                           display=display)


@app.route('/with_score', methods=['GET'])
def with_score():
    i, display = _get_i()
    explanation = _retrieve_explanation_i(i)

    return render_template('recommendation_with_score.html', explanation=explanation, next=(i + 1), previous=(i - 1),
                           display=display)


@app.route('/with_explanation', methods=['GET'])
def with_explanation():
    i, display = _get_i()
    explanation = _retrieve_explanation_i(i)

    return render_template('recommendation_with_explanation.html', explanation=explanation, next=(i + 1),
                           previous=(i - 1),
                           display=display)


@app.route('/without_explanation_with_userfeatures', methods=['GET'])
def without_explanation_with_userfeatures():
    i, display = _get_i()
    explanation = _retrieve_explanation_i(i)
    return render_template('recommendation_without_explanation_with_userfeatures.html', explanation=explanation,
                           next=(i + 1), previous=(i - 1),
                           display=display)


@app.route('/with_explanation_with_userfeatures', methods=['GET'])
def with_explanation_with_userfeatures():
    i, display = _get_i()
    explanation = _retrieve_explanation_i(i)
    return render_template('recommendation_with_explanation_with_userfeatures.html', explanation=explanation,
                           next=(i + 1), previous=(i - 1),
                           display=display)


@app.route('/recommendation_explanation', methods=['GET'])
def recommendation_explanation():
    # FIXME: If requests from unipark all initialize the same session I'll have to solve it differently.
    # e.g. UserId and DB (SQL Lite)
    explanation = _retrieve_next_explanation()

    # # save/ load for debug
    # pickle.dump(explanation, open('explanation.p', 'wb'))
    # explanation = pickle.load(open('explanation.p', 'rb'))
    # explanation = controller._retrieve_next_explanation()

    return render_template('recommendation_with_explanation.html', explanation=explanation)


@app.route('/plot_explanation.png')
def plot_explanation():
    movieId = str(int(float(request.args['movieId'])))
    userId = str(int(float(request.args['userId'])))
    logging.info("PlotIDs are " + userId + " " + movieId)

    image = controller.get_explanation_image(userId, movieId)
    # return ('', 204)
    return Response(image.getvalue(), mimetype='image/png')


@app.route('/user_features')
def user_features():
    try:
        user_features = session['top_user_features']
    except:
        return render_template('error.html', request_url=session['request_url'])

    return render_template('user_features.html', user_features=user_features)


@app.route('/user_features_link_without_explanation')
def user_features_link_without_explanation():
    try:
        user_features = session['top_user_features']
    except:
        return render_template('error.html', request_url = session['request_url'] )

    return render_template('user_features_link_without_explanation.html', user_features=user_features)

@app.route('/user_features/plot_user_features.png')
def plot_user_features_png():
    try:
        user_features = session['top_user_features']
    except:
        return render_template('error.html', request_url=session['request_url'])
    image = controller.draw_top_user_features(user_features)

    # return ('', 204)
    return Response(image.getvalue(), mimetype='image/png')

@app.route('/test')
def test():
    ratings_dev = controller.data_handler.load_ratings_dev()
    ratings = controller.parse_pandas_to_flask(ratings_dev)

    return render_template('test.html', ratings=ratings)

if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [START gae_python37_render_template]
